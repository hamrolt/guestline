import game, { initialState } from "../../reducers/game";
import * as GameActions from "../../actions/game";

describe("game reducer", () => {
  it("should return proper initial state when no action is performed", () => {
    expect(game(undefined, {})).toEqual(initialState);
  });

  it("should indicate game over when all ships are sunk", () => {
    const action = {
      type: GameActions.GAME_STATUS_CHECK,
      payload: { allSunk: true }
    };
    expect(game(undefined, action)).toEqual({
      ...initialState,
      over: true
    });
  });
});
