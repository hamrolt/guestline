import board, { initialState } from "../../reducers/board";
import * as BoardActions from "../../actions/board";
import * as FleetActions from "../../actions/fleet";

describe("board reducer", () => {
  it("should return proper initial state when no action is performed", () => {
    expect(board(undefined, {})).toEqual(initialState);
  });

  it("should mark proper fields as ship when placing vertical ship", () => {
    const row = 5;
    const col = 8;
    const action = {
      type: FleetActions.FLEET_PLACE_SHIP,
      payload: { x: col, y: row, len: 4, dir: 0, id: "s3" }
    };
    const reducer = board(undefined, action);

    for (let i = 0; i < 4; i++) {
      const field = reducer[row + i][col];
      expect(field).toHaveProperty("ship", true);
    }
  });

  it("should mark proper fields as ship when placing horizontal ship", () => {
    const row = 5;
    const col = 6;
    const action = {
      type: FleetActions.FLEET_PLACE_SHIP,
      payload: { x: col, y: row, len: 4, dir: 1, id: "s3" }
    };
    const reducer = board(undefined, action);

    for (let i = 0; i < 4; i++) {
      const field = reducer[row][col + i];
      expect(field).toHaveProperty("ship", true);
    }
  });

  it("should indicate field as struck", () => {
    const row = 5;
    const col = 8;
    const action = {
      type: BoardActions.BOARD_FIELD_STRUCK,
      payload: { x: col, y: row }
    };
    const reducer = board(undefined, action);
    const field = reducer[row][col];

    expect(field).toHaveProperty("struck", true);
  });
});
