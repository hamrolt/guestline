import fleet, { initialState } from "../../reducers/fleet";
import * as FleetActions from "../../actions/fleet";

describe("fleet reducer", () => {
  it("should return proper initial state when no action is performed", () => {
    expect(fleet(undefined, {})).toEqual(initialState);
  });

  it("should instantiate ship with proper id", () => {
    const shipId = "s3";
    const action = {
      type: FleetActions.FLEET_PLACE_SHIP,
      payload: { x: 5, y: 5, len: 4, dir: 0, id: shipId }
    };

    expect(fleet(undefined, action)).toHaveProperty(shipId);
  });

  it("should instantiate ship as not sunk initially", () => {
    const shipId = "s3";
    const action = {
      type: FleetActions.FLEET_PLACE_SHIP,
      payload: { x: 5, y: 5, len: 4, dir: 0, id: shipId }
    };

    expect(fleet(undefined, action)).toHaveProperty([shipId, "sunk"], false);
  });

  it("should instantiate vertical ship with proper segments", () => {
    const shipId = "s3";
    const col = 5;
    const action = {
      type: FleetActions.FLEET_PLACE_SHIP,
      payload: { x: col, y: 5, len: 4, dir: 0, id: shipId }
    };

    expect(fleet(undefined, action)).toHaveProperty(
      [shipId, "segments"],
      [{ x: col, y: 5 }, { x: col, y: 6 }, { x: col, y: 7 }, { x: col, y: 8 }]
    );
  });

  it("should instantiate horizontal ship with proper segments", () => {
    const shipId = "s3";
    const row = 5;
    const action = {
      type: FleetActions.FLEET_PLACE_SHIP,
      payload: { x: 5, y: row, len: 4, dir: 1, id: shipId }
    };

    expect(fleet(undefined, action)).toHaveProperty(
      [shipId, "segments"],
      [{ x: 5, y: row }, { x: 6, y: row }, { x: 7, y: row }, { x: 8, y: row }]
    );
  });

  it("should remove struck segment from ship", () => {
    const shipId = "s3";
    const initialState = {
      s3: {
        segments: [
          { x: 5, y: 5 },
          { x: 5, y: 6 },
          { x: 5, y: 7 },
          { x: 5, y: 8 }
        ],
        sunk: false
      }
    };
    const action = {
      type: FleetActions.FLEET_SHIP_STRUCK,
      payload: { x: 5, y: 7, id: shipId }
    };

    expect(fleet(initialState, action)).toHaveProperty(
      [shipId, "segments"],
      [{ x: 5, y: 5 }, { x: 5, y: 6 }, { x: 5, y: 8 }]
    );
  });

  it("should indicate ship as sunk when last loosing last segment", () => {
    const shipId = "s3";
    const initialState = {
      s3: { segments: [{ x: 5, y: 5 }], sunk: false }
    };
    const action = {
      type: FleetActions.FLEET_SHIP_STRUCK,
      payload: { x: 5, y: 5, id: shipId }
    };

    expect(fleet(initialState, action)).toHaveProperty([shipId, "sunk"], true);
  });
});
