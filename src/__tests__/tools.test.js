import boardReducer from "../reducers/board";
import * as FleetActions from "../actions/fleet";
import { getRandomInt, canPlace } from "../tools";

describe("Generated random integer", () => {
  for (let i = 1; i <= 500; i++) {
    const randomInt = getRandomInt(i);

    it(`should be less than ${i}`, () => {
      expect(randomInt).toBeLessThan(i);
    });

    it(`should be an integer`, () => {
      expect(Number.isInteger(randomInt)).toBe(true);
    });
  }
});

describe("canPlace method", () => {
  it(`should return true when there is no collision`, () => {
    const action = {
      type: FleetActions.FLEET_PLACE_SHIP,
      payload: { x: 5, y: 5, len: 4, dir: 0, id: "s3" }
    };
    const board = boardReducer(undefined, action);
    const canPlaceShip = canPlace(board, 6, 5, 2, 1);

    expect(canPlaceShip).toBe(true);
  });

  it(`should return false when collision occurs`, () => {
    const action = {
      type: FleetActions.FLEET_PLACE_SHIP,
      payload: { x: 5, y: 5, len: 4, dir: 0, id: "s3" }
    };
    const board = boardReducer(undefined, action);
    const canPlaceShip = canPlace(board, 5, 5, 2, 1);

    expect(canPlaceShip).toBe(false);
  });
});
