import * as GameActions from "../actions/game";

export const initialState = {
  over: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GameActions.GAME_STATUS_CHECK:
      return {
        ...state,
        over: action.payload.allSunk
      };

    default:
      return state;
  }
};
