import * as FleetActions from "../actions/fleet";
import * as BoardActions from "../actions/board";

const createBoard = (width, height = width) => {
  const board = [];
  for (let rowIndex = 0; rowIndex < height; rowIndex++) {
    const row = [];
    for (let colIndex = 0; colIndex < width; colIndex++) {
      row.push({ struck: false });
    }
    board.push(row);
  }
  return board;
};

export const initialState = createBoard(10);

export default (state = initialState, action) => {
  switch (action.type) {
    case FleetActions.FLEET_PLACE_SHIP: {
      const newState = [...state];

      for (let i = 0; i < action.payload.len; i++) {
        const field = action.payload.dir
          ? newState[action.payload.y][action.payload.x + i]
          : newState[action.payload.y + i][action.payload.x];
        field["ship"] = true;
        field["id"] = action.payload.id;
        field["debug"] = false;
      }

      return newState;
    }

    case BoardActions.BOARD_FIELD_STRUCK: {
      const newState = [...state];
      const field = newState[action.payload.y][action.payload.x];

      field["struck"] = true;
      return newState;
    }

    default:
      return state;
  }
};
