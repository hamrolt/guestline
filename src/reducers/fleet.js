import * as FleetActions from "../actions/fleet";

export const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case FleetActions.FLEET_PLACE_SHIP: {
      const segments = [];

      for (let i = 0; i < action.payload.len; i++) {
        segments.push({
          x: action.payload.dir === 1 ? action.payload.x + i : action.payload.x,
          y: action.payload.dir === 0 ? action.payload.y + i : action.payload.y
        });
      }

      return {
        ...state,
        [action.payload.id]: {
          segments,
          sunk: false
        }
      };
    }

    case FleetActions.FLEET_SHIP_STRUCK: {
      const ship = state[action.payload.id];
      const segments = ship.segments.filter(segment => {
        return segment.x !== action.payload.x || segment.y !== action.payload.y;
      });

      return {
        ...state,
        [action.payload.id]: {
          ...state[ship],
          segments,
          sunk: !Boolean(segments.length)
        }
      };
    }

    default:
      return state;
  }
};
