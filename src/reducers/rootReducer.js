import { combineReducers } from "redux";
import game from "./game";
import board from "./board";
import fleet from "./fleet";

export default combineReducers({
  game,
  board,
  fleet
});
