const ships = [
  { id: "s1", type: "battleship", segments: 5 },
  { id: "s2", type: "destroyer", segments: 4 },
  { id: "s3", type: "destroyer", segments: 4 }
];

const cols = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];
const rows = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"];

export { ships, cols, rows };
