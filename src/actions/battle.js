import { checkStatus } from "./game";
import { fieldStruck } from "./board";
import { shipStruck } from "./fleet";

export const fire = (x, y) => (dispatch, getState) => {
  const shipId = getState().board[y][x].id;

  dispatch(fieldStruck(x, y));

  if (shipId) {
    dispatch(shipStruck(x, y, shipId));
  }

  dispatch(checkStatus());
};
