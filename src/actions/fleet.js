import { randomizeField, canPlace } from "../tools";
export const FLEET_PLACE_SHIP = "FLEET_PLACE_SHIP";
export const FLEET_SHIP_STRUCK = "FLEET_SHIP_STRUCK";

export const shipStruck = (x, y, id) => dispatch => {
  dispatch({
    type: FLEET_SHIP_STRUCK,
    payload: { x, y, id }
  });
};

export const placeShip = (x, y, len, dir, id) => dispatch => {
  dispatch({
    type: FLEET_PLACE_SHIP,
    payload: { x, y, len, dir, id }
  });
};

export const placeShips = ships => (dispatch, getState) => {
  for (let ship of ships) {
    const board = getState().board;
    let x = 0;
    let y = 0;
    let dir = 0;
    let isPlaceFree = false;

    do {
      ({ x, y, dir } = randomizeField(10, 10, ship.segments));
      isPlaceFree = canPlace(board, x, y, ship.segments, dir);
    } while (!isPlaceFree);

    dispatch(placeShip(x, y, ship.segments, dir, ship.id));
  }
};
