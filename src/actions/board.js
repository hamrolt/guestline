export const BOARD_FIELD_STRUCK = "BOARD_FIELD_STRUCK";

export const fieldStruck = (x, y) => dispatch => {
  dispatch({
    type: BOARD_FIELD_STRUCK,
    payload: { x, y }
  });
};
