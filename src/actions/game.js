export const GAME_STATUS_CHECK = "GAME_STATUS_CHECK";

export const checkStatus = () => (dispatch, getState) => {
  const fleet = Object.values(getState().fleet);
  let allSunk = fleet.every(ship => ship.sunk === true);

  dispatch({
    type: GAME_STATUS_CHECK,
    payload: { allSunk }
  });
};
