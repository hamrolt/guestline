import React from "react";

const Select = ({ name, value, options, onChange }) => {
  return (
    <select name={name} onChange={onChange} value={value}>
      {options.map((option, index) => (
        <option key={`${name}.${index}`} value={index}>
          {option}
        </option>
      ))}
    </select>
  );
};

export default Select;
