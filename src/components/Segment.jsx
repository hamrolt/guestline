import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  grid-row: ${props => props.row};
  grid-column: ${props => props.col};
  width: 40px;
  height: 40px;
  background-color: ${props => {
    let color = "#add8e6";
    if (props.struck && !props.ship) {
      color = "#f03a47";
    }
    if (props.struck && props.ship && !props.sunk) {
      color = "#6daedb";
    }
    if (props.struck && props.ship && props.sunk) {
      color = "#000";
    }
    return color;
  }};
  outline: ${props => (props.debug ? "1px dashed darkblue" : null)};
`;

const Segment = props => <Wrapper {...props} />;

export default Segment;
