import React, { useCallback } from "react";
import styled from "styled-components";
import Segment from "./Segment";
import { cols, rows } from "../setup";

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(${props => props.segmentCount}, 1fr);
  grid-template-rows: repeat(${props => props.segmentCount}, 1fr);
  grid-gap: 1px;
  width: ${props =>
    (props.segmentCount + 2) * props.segmentSize + (props.segmentCount - 1)}px;
  height: ${props =>
    (props.segmentCount + 2) * props.segmentSize + (props.segmentCount - 1)}px;
  margin: 20px;
`;

const Marker = styled.div`
  grid-row: ${props => props.row};
  grid-column: ${props => props.col};
  display: flex;
  justify-content: center;
  align-items: center;
  width: ${props => props.size}px;
  height: ${props => props.size}px;
  background-color: ${props => (props.active ? "#e5e5e5" : "#f5f5f5")};
  box-shadow: ${props => (props.active ? "inset" : "")} 1px 1px 0px 1px
    rgba(0, 0, 0, 0.2);
`;

const Grid = ({
  board,
  active,
  fleet,
  segmentSize,
  segmentCount,
  onSelectX,
  onSelectY
}) => {
  const handleSelectX = useCallback(index => () => onSelectX(index), [
    onSelectX
  ]);
  const handleSelectY = useCallback(index => () => onSelectY(index), [
    onSelectY
  ]);

  return (
    <Wrapper segmentSize={segmentSize} segmentCount={segmentCount}>
      {cols.map((item, index) => (
        <Marker
          key={`tc${index}`}
          row="1"
          col={index + 2}
          size={segmentSize}
          active={active.x === index}
          onClick={handleSelectX(index)}
        >
          {item}
        </Marker>
      ))}
      {rows.map((item, index) => (
        <Marker
          key={`lr${index}`}
          row={index + 2}
          col="1"
          size={segmentSize}
          active={active.y === index}
          onClick={handleSelectY(index)}
        >
          {item}
        </Marker>
      ))}
      {board.map((row, rowIndex) => {
        return row.map((segment, colIndex) => {
          const sunk = fleet[segment.id] ? fleet[segment.id].sunk : false;

          return (
            <Segment
              key={`r${rowIndex}c${colIndex}`}
              row={rowIndex + 2}
              col={colIndex + 2}
              sunk={sunk}
              {...segment}
            />
          );
        });
      })}
      {cols.map((item, index) => (
        <Marker
          key={`tc${index}`}
          row="12"
          col={index + 2}
          size={segmentSize}
          active={active.x === index}
          onClick={handleSelectX(index)}
        >
          {item}
        </Marker>
      ))}
      {rows.map((item, index) => (
        <Marker
          key={`lr${index}`}
          row={index + 2}
          col="12"
          size={segmentSize}
          active={active.y === index}
          onClick={handleSelectY(index)}
        >
          {item}
        </Marker>
      ))}
    </Wrapper>
  );
};

export default Grid;
