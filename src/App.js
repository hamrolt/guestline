import React, { useState, useCallback } from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import { cols, rows } from "./setup";
import Grid from "./components/Grid";
import Select from "./components/Select";
import * as BattleActions from "./actions/battle";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Controls = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 160px;
`;

function App({ game, board, fleet, fire }) {
  const [coordX, setCoordX] = useState(0);
  const [coordY, setCoordY] = useState(0);

  const handleCoordXChange = useCallback(
    e => setCoordX(Number.parseInt(e.target.value, 10)),
    [setCoordX]
  );
  const handleCoordYChange = useCallback(
    e => setCoordY(Number.parseInt(e.target.value, 10)),
    [setCoordY]
  );

  const handleFireClick = useCallback(
    (coordX, coordY) => () => fire(coordX, coordY),
    [fire]
  );

  return (
    <Wrapper>
      <Grid
        board={board}
        fleet={fleet}
        active={{ x: coordX, y: coordY }}
        segmentSize={40}
        segmentCount={10}
        onSelectX={setCoordX}
        onSelectY={setCoordY}
      />
      {game.over ? (
        <div>GAME OVER</div>
      ) : (
        <Controls>
          <Select
            name="coordX"
            options={cols}
            value={coordX}
            onChange={handleCoordXChange}
          />
          <Select
            name="coordY"
            options={rows}
            value={coordY}
            onChange={handleCoordYChange}
          />
          <button tyoe="button" onClick={handleFireClick(coordX, coordY)}>
            FIRE
          </button>
        </Controls>
      )}
    </Wrapper>
  );
}

const mapStateToProps = ({ game, board, fleet }) => ({
  game,
  board,
  fleet
});

const mapDispatchToProps = {
  fire: BattleActions.fire
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
