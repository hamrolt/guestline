const getRandomInt = max => {
  return Math.floor(Math.random() * Math.floor(max));
};

const randomizeField = (width, height, bleed) => {
  let x = 0;
  let y = 0;
  const dir = getRandomInt(2);

  if (dir) {
    width = width - bleed;
  } else {
    height = height - bleed;
  }

  x = getRandomInt(width);
  y = getRandomInt(height);

  return { x, y, dir };
};

const canPlace = (board, x, y, len, dir) => {
  let collision = false;
  for (let offset = 0; offset < len; offset++) {
    collision = dir
      ? board[y][x + offset].ship === true
      : board[y + offset][x].ship === true;

    if (collision) {
      return false;
    }
  }
  return true;
};

export { getRandomInt, randomizeField, canPlace };
