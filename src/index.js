import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import store from "./store";
import App from "./App";
import { ships } from "./setup";

import * as FleetActions from "./actions/fleet";

store.dispatch(FleetActions.placeShips(ships));

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
