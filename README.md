App:

- `yarn` to install dependencies
- `yarn start` to run the app
- `create react app` has been used to speed up development within well-known environment
- `debug` is available by setting a `debug` flag in board reducer under `FLEET_PLACE_SHIP`

Interface:

- simplistic, non-artistic approach has been chosen as only the design provided by designers could be a single source of truth for a task
- field controls and dropdowns are used instead of classic input field to increase usability
- field controls are indicating current choice and can be switched / altered together with drop downs
- fire button could follow coordinates when both selected but form the other hand `agile` software is not about creating unspecified functionalities

Styling:

- `styled components` are used as there is some dynamic logic involved in styling (e.g. grid placement, various field indications etc) and using regular classes and static styling could lead into issues
- styles are placed within components as this is a simple case, but in some more complex case the should be extracted to separate files to increase readability

Testing:

- `yarn test` to run tests
- tool methods are tested for accuracy and liability
- all reducers are unit tested to be sure the app logic meets the brief
- action creators are not covered as they are simple in this case, but in some more complex case they should be covered as well

Accessibility:

- in regular case there is a front-end duty to keep the interface and data accessible for disabled people, but not all of cases are obliged to implement it and I assume there was no need for doing this as there was no clear indication or mention
